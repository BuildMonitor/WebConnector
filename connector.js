Connector =
{
    _refreshTimer: null,
    _refreshing: false,

    _projects: null,

    _lastUrl: null,

    FormData:
    {
        Name: { Type: "text", Title: "Name", Value: "Web Connector" },
        Urls: { Type: "multiLineText", Title: "URLs", Required: true, Restart: true, ProtocolCheck: true },
        RegexSuccess: { Type: "regex", Title: "Success Regex", Value: "(online)" },
        RegexFailed: { Type: "regex", Title: "Failed Regex", Value: "^((?!online)[\\s\\S])*$" },
        RegexRunning: { Type: "regex", Title: "Running Regex", Value: "(starting)" },
        ProjectImage: { Type: "regex", Title: "Project Image (regex or absolute URL)", Value: "(https?:\\/\\/.*\\.(?:png|jpg|jpeg))" },
        LabelSuccess: { Type: "text", Title: "Label for Success", Value: "Online" },
        LabelFailed: { Type: "text", Title: "Label for Failed", Value: "Offline" },
        LabelRunning: { Type: "text", Title: "Label for Running", Value: "Starting" },
        Interval: { Type: "number", Title: "Request Interval (in milliseconds)", Value: 1000, Required: true }
    },

    Index: 0,

    Logger: null,
    ConnectorConfig: null,
    Requestor: null,
    DataSink: null,

    Initialize: function()
    {
        this._projects = { };
    },

    Start: function()
    {
        if (this._refreshTimer != null)
        {
            this.Logger.Log("Already started.", this.Logger.LogLevels.Warning, this);
            return;
        }

        for (var url in this.ConnectorConfig.Urls)
        {
            var project = {
                Identifier: this.DataSink.GetValidIdentifier(url),
                Status: "Unknown",
                ImportantRefStatus: "Success"
            };
            this.ApplyUrlParts(project, url);

            if (!project.Identifier)
            {
                continue;
            }

            this._projects[project.Identifier] = project;
        }

        this.DataSink.Update(this._projects, null, this);

        this._refreshTimer = setInterval(this.Refresh.bind(this), this.ConnectorConfig.Interval);
        this.Refresh();
    },

    Stop: function()
    {
        if (this._refreshTimer == null)
        {
            return;
        }

        clearInterval(this._refreshTimer);
        this._refreshTimer = null;
    },

    Refresh: function()
    {
        if (this._refreshing)
        {
            this.Logger.Log("Already refreshing, skipping...", this.Logger.LogLevels.Info, this);
            return;
        }

        this.RequestUrls(this.ConnectorConfig.Urls);
    },

    RequestUrls: function(urls)
    {
        var useNext = !this._lastUrl;
        var first = null;
        for (var url in urls)
        {
            if (!first)
            {
                first = url;
            }

            if (useNext)
            {
                this.RequestUrl(url);
                return;
            }

            if (url === this._lastUrl)
            {
                useNext = true;
            }
        }

        if (first)
        {
            this.RequestUrl(first);
        }
    },

    RequestUrl: function(url)
    {
        this._refreshing = true;
        this._lastUrl = url;

        var options = {
            DataType: "text",
            MimeType: "text/plain"
        };

        var self = this;
        this.Requestor.Get(url,
            function(data)
            {
                self.ParseResponse.call(self, url, data);
            },
            function(jqXhr, textStatus, errorThrown)
            {
                self.RequestFailed.call(self, url, textStatus, errorThrown);
            },
            function()
            {
                self._refreshing = false;
            },
            options
        );
    },

    RequestFailed: function(url)
    {
        this._refreshing = false;

        var project = {
            Identifier: this.DataSink.GetValidIdentifier(url),
            Status: "Failed",
            StatusLabel: "Connection failed",
            Username: "Connection failed",
            ImportantRefStatus: "Failed"
        };
        this.ApplyUrlParts(project, url);

        this._projects[project.Identifier] = project;
        this.DataSink.Update(this._projects, project.Identifier, this);
    },

    ApplyUrlParts: function(project, url)
    {
        if (url.indexOf("://") < 0)
        {
            project.Name = url;
            return;
        }

        // eslint-disable-next-line no-magic-numbers
        project.Name = url.substr(url.indexOf("://") + 3);
        if (project.Name.indexOf("/") >= 0)
        {
            project.Reference = project.Name.substr(project.Name.indexOf("/") + 1);
            project.Name = project.Name.substr(0, project.Name.indexOf("/"));
        }
    },

    // eslint-disable-next-line complexity
    ParseResponse: function(url, data)
    {
        var project = {
            Identifier: this.DataSink.GetValidIdentifier(url)
        };
        if (!project.Identifier)
        {
            return;
        }

        this.ApplyUrlParts(project, url);

        this._projects[project.Identifier] = project;

        try
        {
            var matchRunning = data.match(this.ConnectorConfig.RegexRunning);
            var matchSuccess = data.match(this.ConnectorConfig.RegexSuccess);
            var matchFailed = data.match(this.ConnectorConfig.RegexFailed);

            if (this.ConnectorConfig.RegexRunning && matchRunning)
            {
                project.Status = "Running";
                project.StatusLabel = (this.ConnectorConfig.LabelRunning || "").replace(/\$Match/g, matchRunning[0]);
            }
            else if (this.ConnectorConfig.RegexFailed && matchFailed)
            {
                project.Status = "Failed";
                project.StatusLabel = (this.ConnectorConfig.LabelFailed || "").replace(/\$Match/g, matchFailed[0]);
            }
            else if (this.ConnectorConfig.RegexSuccess && matchSuccess)
            {
                project.Status = "Success";
                project.StatusLabel = (this.ConnectorConfig.LabelSuccess || "").replace(/\$Match/g, matchSuccess[0]);
            }
            else
            {
                project.Status = "Unknown";
            }
        }
        catch (exception)
        {
            project.Status = "Failed";
            project.StatusLabel = "Regex-Error: " + exception;
            this.Logger.Log("Regex-Error: " + exception, this.Logger.LogLevels.Error, this);
        }

        try
        {
            if (this.ConnectorConfig.ProjectImage.indexOf("http") === 0)
            {
                project.Image = this.ConnectorConfig.ProjectImage;
            }
            else
            {
                var match = data.match(this.ConnectorConfig.ProjectImage);
                if (match && match[0])
                {
                    project.Image = match[0];
                }
            }
        }
        catch (exception)
        {
            project.Image = null;
            this.Logger.Log("Project Image Regex-Error: " + exception, this.Logger.LogLevels.Error, this);
        }

        project.ImportantRefStatus = project.Status;
        project.Username = project.Status;

        this.DataSink.Update(this._projects, project.Identifier, this);
    }
};